/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Expected Output
//1,-2,6,-15,31,-56,92.....n

import java.util.Scanner;
public class Main
{
	public static void main(String[] args) {
	    //Initialization
		int i = 1;
		int sum = 1;
		int sign = 1;
		
		//User Input
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		//Iteration
		while(sum<=n){
		
		//Output
		    System.out.println(sum * sign);
		    sum = sum+(i*i);
		    sign = sign * -1;
		    i++;
		}
	}
}
