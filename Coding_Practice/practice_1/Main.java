/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Expected Output
//1,-3,5,-7,9,-11,13......n
import java.util.Scanner;
public class Main
{
	public static void main(String[] args) {
	    //Initialization
	    int sign = 1;
	    
	    //Taking input from user
	    Scanner sc = new Scanner(System.in);
	    int n = sc.nextInt();
	    
	    //Iteration
	    for(int i=1; i<=n; i++){
	        if(i%2 != 0){
	    
	    //Output        
	            System.out.println(i*sign);
	            sign = sign * -1;
	        }
	    }
	}
}
